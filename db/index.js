const fs = require('fs-extra');
const path = require('path');

const filedir = `${__dirname}/`;

const sessions = () => {
  const filename = path.join(filedir, 'sessions.json');
  try {
      fs.writeJsonSync(filename,[],{flag:'ax'})
  }
  catch (ex)
  {
      //file already exists great!
  }

  return ({
    select: ( sessionId ) =>
      fs.readJson(filename).then(d => d.find(r => r.sessionId === sessionId)),
    upsert: (sessionId, data ) => {
      fs.readJson(filename).then(d => {
        let sessions = d.filter(r => r.sessionId != sessionId)
        let newSessionValue = { sessionId , data }
        sessions.push(newSessionValue)
        return fs.writeJson(filename,sessions).then(newSessionValue)
      });
    }
    })
};


module.exports = sessions