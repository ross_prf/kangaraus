// server.js
const express = require('express');
const app = express();
const requestPromise = require('request-promise');
const stringSimilarity = require('string-similarity');
const db = require('./db');

const path  = require('path');
const fs = require("fs");
const data = JSON.parse(fs.readFileSync(path.join(__dirname, '.', 'languages.json'), 'utf8'));

app.use(express.static('public'));

app.get("/", function (request, response) {
  response.sendFile(__dirname + '/index.html');
});

app.get("/sessions", function (request, response) {
  response.sendFile(__dirname + '/db/sessions.json');
});

app.get("/question", async (request, response) => {
  const query = request.query;
  const sessionId = query["messenger user id"];

  let session = await db().select(sessionId);
  if (!session) {
    session = {
      sessionId: sessionId
    };
  }

  if (!session.data || session.data.count > 4) {
    session.data = {
      count: 0,
      score: 0,
      prevAnswer: {}
    };
  }
 
  const question = pickRandomQuestion();
  session.data.prevAnswer = question;

  await db().upsert(session.sessionId, session.data);
 
  response.json({
      messages: [
        {
          text: question.question
        }
      ]
    });
});

app.get("/answer", async (request, response) => {
  const query = request.query;
  const sessionId = query["messenger user id"];
  const answerReq = query["answer"];
  
  let session = await db().select(sessionId);

  const prevAnswer = session.data.prevAnswer

  const language = prevAnswer.solution.language;
  const word = prevAnswer.solution.word;
  const english = prevAnswer.solution.english;
  session.data.count = session.data.count + 1;

  let answerResponse;

  const matches = stringSimilarity.findBestMatch(answerReq, ['true', 'false']);
  const answer = matches.bestMatch.target;

  if (answer == session.data.prevAnswer.answer.toString().toLowerCase()) {
    session.data.score = session.data.score + 1;
    answerResponse = `Yeeeey, thats correct! The word ${word} in ${language} means ${english}`;
  } else {
    answerResponse = `Buuuu! That's incorrect! The word ${word} in ${language} means ${english}`;
  }

  let finalResponse;
  if (session.data.score === 3) {
    session.data.count = 0;
    session.data.score = 0;
    finalResponse = `You won the 3 questions!`;
    gifResponse = "https://media.giphy.com/media/vmon3eAOp1WfK/giphy.gif"
  } else {
    if (session.data.count === 3) {
      const total = 3 - session.data.score
      finalResponse = `You lose ${total} out of the 3 questions! Better luck next time!!`;
      gifResponse = "https://media.giphy.com/media/NTY1kHmcLsCsg/giphy.gif"
      session.data.count = 0;
      session.data.score = 0;
    }
  }

  await db().upsert(session.sessionId, session.data);

  const apiResponse = {
        messages: [
          { text: answerResponse },
        ]
      }
  ;

  if (finalResponse) {
    apiResponse.messages.push({ text: finalResponse });
    apiResponse.messages.push({
      "attachment": {
        "type": "image",
        "payload": {
          "url": gifResponse
        }
      }
    });
  }

  response.json(apiResponse);
});




const pickRandomQuestion = () => {
  const randomLanguage = Math.floor(Math.random() * data.length);
  const language = data[randomLanguage]
  const randomWord = Math.floor(Math.random() * language.words.length);
  const word = language.words[randomWord].word;
  const english = language.words[randomWord].english;

  const randomFakeLanguage = Math.floor(Math.random() * data.length);
  const fakeLanguage = data[randomFakeLanguage]
  const randomFakeWord = Math.floor(Math.random() * fakeLanguage.words.length);
  const fakeWord = fakeLanguage.words[randomFakeWord].word;
  
  let questionLanguage = language.language;
  let questionWord;
  let questionAnswer;

  const trueFalse = Math.random() < 0.5;

  if(trueFalse) {
    questionWord = word;
    questionAnswer = true;
  } else {
    questionWord = fakeWord;
    questionAnswer = false;
  }

  const question = `"${questionWord}" in ${questionLanguage} means ${english} in English`;
  const prevAnswer = {
    question: question,
    answer: questionAnswer,
    solution: {
      language: language.language,
      word,
      english
    }
  };

  return prevAnswer;
}

app.get('/api', function(request, response) {
    console.log(request);
    response.json({
        messages: [
            {text: 'Welcome!'}
        ]
      });
});

app.get('/translate', function(request, response) {
  const languages = data.map(x => x.language)

  const { translateReq } = request.query;
  const words = translateReq.split(" to ");

  if (words.length !== 2) {
    return response.json({
      messages: [
        { text: 'Please write it in the form of "word to language"' },
        { text: 'Try again!' }
      ]
    });
  }

  englishWordReq = words[0].toLowerCase();
  languageReq = words[1].toLowerCase();

  const matches = stringSimilarity.findBestMatch(languageReq, languages);
  const language = matches.bestMatch.target.toLowerCase();

  let result  = data.find(x => {
    return x.language.toLowerCase() == language
    }).words.find(y => {
        return y.english.toLowerCase() == englishWordReq
    })
   
  let answer;

  if (result) {
    answer = `${englishWordReq} in ${language} is ${result.word}`
    if (result.pronunciation) {
      answer = `${answer} and is pronounced ${result.pronunciation}`;
    }
  } else {
    answer =  `Sorry! We don't know that word`;
  }
  
  response.json({
    messages: [
      { text: answer }
    ]
  });
});

// listen for requests :)
var listener = app.listen(process.env.PORT || 3000, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});